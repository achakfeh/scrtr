//
//  GoogleTranslateAppDelegate.h
//  GoogleTranslate
//
//  Created by Ray Wenderlich on 7/12/10.
//  Copyright Ray Wenderlich 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GoogleTranslateViewController;

@interface GoogleTranslateAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    GoogleTranslateViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet GoogleTranslateViewController *viewController;

@end

