//
//  GoogleTranslateViewController.h
//  GoogleTranslate
//
//  Created by Ray Wenderlich on 7/12/10.
//  Copyright Ray Wenderlich 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoogleTranslateViewController : UIViewController {
    IBOutlet UITextView *textView;
    IBOutlet UITextField *textField;
    IBOutlet UIButton *button;
    NSMutableData *responseData;
    NSMutableArray *translations;
    NSString *_lastText;
    IBOutlet UITextView *textViewInput;
}

@property (nonatomic, copy) NSString * lastText;

- (IBAction)doTranslation;

@end

