//
//  GoogleTranslateViewController.m
//  GoogleTranslate
//
//  Created by Ray Wenderlich on 7/12/10.
//  Copyright Ray Wenderlich 2010. All rights reserved.
//

#import "GoogleTranslateViewController.h"
#import "JSON.h"

@implementation GoogleTranslateViewController
@synthesize lastText = _lastText;


/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


- (void)viewDidLoad {
    [super viewDidLoad];
    translations = [[NSMutableArray alloc] init]; 
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
    [translations release];
    [_lastText release];
    [textViewInput release];
    [super dealloc];
}

- (void)performTranslation {
    
    // Break if we exceed limit
    if (translations.count > 50) {
        button.enabled = YES;
        return;
    }
    
    responseData = [[NSMutableData data] retain];
    BOOL translateToEnglish = ([translations count] % 2 == 0);
    
    // Bail if we have a match
    if (!translateToEnglish && [translations count] >= 3) {
        NSString *oldString = [translations objectAtIndex:[translations count] - 3];
        if ([oldString caseInsensitiveCompare:_lastText] == NSOrderedSame) {
            button.enabled = YES;
            return;
        }
    }
    
    NSString *langString;
    if (translateToEnglish) {
        langString = @"ja|en";        
    } else {
        langString = @"en|ja";
    }
    
    NSString *textEscaped = [_lastText stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
   // NSString *langStringEscaped = [langString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

    
    NSString *url = [NSString stringWithFormat:@"https://translate.google.com/translate_a/single?client=t&sl=auto&tl=de&dt=t&ie=UTF-8&oe=UTF-8&oc=1&otf=1&ssel=3&tsel=3&q=%@",textEscaped];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    [[NSURLConnection alloc] initWithRequest:request delegate:self];
    button.enabled = YES;
}

- (IBAction)doTranslation {
    
    [translations removeAllObjects];
    [textField resignFirstResponder];
    
    button.enabled = NO;
    self.lastText = textViewInput.text;
    [translations addObject:_lastText];
    textView.text = _lastText;    
    
    [self performTranslation];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [responseData setLength:0];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [responseData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    textView.text = [NSString stringWithFormat:@"Connection failed: %@", [error description]];
    button.enabled = YES;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    [connection release];
    
    NSString *responseString = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
    [responseData release];
    //NSString* regexString =@"^(\\[\\[\\[)$\",,";
    NSString* regexString =@"\\[\\[\\[.+\",,";
    NSRegularExpression *regex =
    [NSRegularExpression regularExpressionWithPattern:regexString
                                              options:NSRegularExpressionCaseInsensitive
                                                error:nil];
    NSRange range = NSMakeRange(0,responseString.length);
    NSArray *translatedSentence=[regex matchesInString:responseString options:0 range:range] ;
  NSString* matchText = [responseString  substringWithRange:[[translatedSentence objectAtIndex:0] range]];
    NSLog(@"%@",matchText);
    
    regexString=@"\".+\"";

    NSArray *array = [matchText componentsSeparatedByString:@","];
    NSRegularExpression *regex1 =[NSRegularExpression regularExpressionWithPattern:regexString
                                              options:NSRegularExpressionCaseInsensitive
                                                error:nil];
            NSString *result=@"";
    for (int i=0; i<[array count];i++) {
        range = NSMakeRange(0, [[array objectAtIndex:i] length]);
        NSArray *arr1=[regex1 matchesInString:[array objectAtIndex:i] options:0 range:range] ;

    if([arr1 count]>0)
    {
        NSString* txt = [[array objectAtIndex:i]  substringWithRange:[[arr1 objectAtIndex:0] range]];
        result=[NSString stringWithFormat:@"%@\n%@",result,txt];
    }
    }
    
    
    textView.text=result;
    return;
    NSMutableDictionary *luckyNumbers = [responseString JSONValue];
    [responseString release];
    if (luckyNumbers != nil) {
        
        NSDecimalNumber * responseStatus = [luckyNumbers objectForKey:@"responseStatus"];
        if ([responseStatus intValue] != 200) {
            button.enabled = YES;
            return;
        }
        
        NSMutableDictionary *responseDataDict = [luckyNumbers objectForKey:@"responseData"];
        if (responseDataDict != nil) {
            NSString *translatedText = [responseDataDict objectForKey:@"translatedText"];
            [translations addObject:translatedText];
            self.lastText = translatedText;            
            textView.text = [textView.text stringByAppendingFormat:@"\n%@", translatedText];
            button.enabled = YES;
            [self performTranslation];
        }
    }
    
}

@end
